# -*- coding: utf-8 -*-
#   receiver-box
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from gpiozero import OutputDevice

from controller import HardwareController

class FemtoAmplifier(HardwareController):
    """Class to control a FEMTO DHPVA-200 amplifier via the digital control
    interface.
    """
    def __init__(self, gpio_config, name=None):
        """Initialize the FemtoAmplifier controller.

        Parameters
        ----------
        gpio_config: A dictionary containg the pin configuration for the amplifier
        name: The name of the amplifier (for use in log files)
        """
        self.gpio_config = gpio_config
        self.log = logging.getLogger(name or 'FemtoAmplifier')
        self.initialized = False

    def init(self):
        # Coupling and bandwidth
        self.gpio_coupling  = OutputDevice(self.gpio_config['coupling'])
        self.gpio_bandwidth = OutputDevice(self.gpio_config['bandwidth'])

        # Gain control
        self.gpio_gain_lsb = OutputDevice(self.gpio_config['gain_lsb'])
        self.gpio_gain     = OutputDevice(self.gpio_config['gain'])
        self.gpio_gain_msb = OutputDevice(self.gpio_config['gain_msb'])

        self.initialized = True
        self.log.debug("initialized.")

    def close(self):
        if self.initialized:
            self.gpio_coupling.close()
            self.gpio_bandwidth.close()
            self.gpio_gain_msb.close()
            self.gpio_gain.close()
            self.gpio_gain_lsb.close()

    @property
    def gain(self):
        """Get the amplifier gain setting in dB.

        The gain is specified with Pin 10-12 of the digital control interface.
        The pins implement three bits of an integer and the gain value can be
        set according to the table:

        Gain  |  Pin 10   Pin 11   Pin 12
        ======|==========================
        10 dB |  LOW      LOW      LOW
        20 dB |  HIGH     LOW      LOW
        30 dB |  LOW      HIGH     LOW
        40 dB |  HIGH     HIGH     LOW
        50 dB |  LOW      LOW      HIGH
        60 dB |  HIGH     LOW      HIGH

        """
        index = int(2 << 0 * self.gpio_gain_lsb.value +
                    2 << 1 * self.gpio_gain.value     +
                    2 << 2 * self.gpio_gain_msb.value)
        return 10*(index + 1)

    @gain.setter
    def gain(self, value):
        """Set the amplifier gain setting in dB."""
        value = int(value / 10) * 10 # Round to multiple of 10
        index = int(value / 10) - 1

        self.gpio_gain_lsb.value = index & 1
        self.gpio_gain.value     = (index >> 1) & 1
        self.gpio_gain_msb.value = (index >> 2 ) & 1

        self.log.info("Set gain to %d dB", value)

    @property
    def coupling(self):
        """Get the current amplifier coupling setting.

        Bandwidth  |  Pin 13
        ===========|========
        AC         |  LOW
        DC         |  HIGH
        """
        if self.gpio_coupling.value:
            return 'DC'
        else:
            return 'AC'

    @coupling.setter
    def coupling(self, value):
        """Set the amplifier to AC or DC coupling."""
        value = str(value)
        if value.upper() == 'AC':
            self.gpio_coupling.off()
        elif value.upper() == 'DC':
            self.gpio_coupling.on()
        else:
            raise ValueError("Coupling can only be 'AC' or 'DC', "
                             "got '{:s}'".format(value))

        self.log.info("Set coupling to %s", value.upper())

    @property
    def bandwidth(self):
        """Get the amplifier bandwidth filter setting in MHz.

        Bandwidth  |  Pin 14
        ===========|========
        20 MHz     |  LOW
        200 MHz    |  HIGH
        """
        if self.gpio_coupling.value:
            return 200
        else:
            return 20

    @bandwidth.setter
    def bandwidth(self, value):
        """Set the amplifier filter bandwidth in MHz."""
        value = int(value)
        if value == 20:
            self.gpio_bandwidth.off()
        elif value == 200:
            self.gpio_bandwidth.on()
        else:
            raise ValueError("Bandwidth can only be 20 or 200, "
                             "got '{:d}'".format(value))

        self.log.info("Set bandwidth to %d MHz", value)

