# -*- coding: utf-8 -*-
#   receiver-box
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from time import sleep
from gpiozero import OutputDevice

from controller import HardwareController

class LatchingSwitch(HardwareController):
    """Class to control a Keysight 8765B latching switch.

    The output is selected by applying a 30ms long current pulse (15 V, ~300mA) to the
    """

    def __init__(self, gpio_config, initial_state=1, name=None, callback=None):
        """Initialize the switch controller.

        Parameters
        ----------
        gpio_config: Dictionary containing the GPIO configuration.
        initial_sate: Initial state of the switch (RF state 1 or 2).
        name: Name of the device for logging purposes
        callback: Function that gets called when the state of the switch
                  changes. Takes an integer parameter to indicate the path.
        """
        self.gpio_config = gpio_config
        self.initial_state = initial_state
        self.current_state = 0
        self.log = logging.getLogger(name or 'Switch')
        self.callback = callback

    def init(self):
        self.gpio_rf1 = OutputDevice(self.gpio_config['rf1'])
        self.gpio_rf2 = OutputDevice(self.gpio_config['rf2'])
        self.state = self.initial_state
        self.log.debug("initialized.")

    @property
    def state(self):
        """Returns the current output state of the switch (1 or 2)"""
        return self.current_state

    @state.setter
    def state(self, value):
        """Sets the new output state."""
        if value not in (1, 2):
            raise ValueError("RF state has to be 1 or 2")

        # Only apply pulse if we need switching
        if value == self.current_state:
            return

        gpio = self.gpio_rf1 if value == 1 else self.gpio_rf2

        gpio.on()
        sleep(0.05)
        gpio.off()

        self.current_state = value

        if self.callback:
            self.callback(self.current_state)

        self.log.info("Switched to rf path %d", value)

