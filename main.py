# -*- coding: utf-8 -*-
#   receiver-box
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import socketserver

from gpiozero import LED, Button

from femto import FemtoAmplifier
from pin_switch import PinSwitch
from switch import LatchingSwitch

class BadRequest(Exception):
    pass

class MainController(object):
    """This is the main controller class.

    It handles events (button presses) and dispatches requests coming in via a
    network connection.
    """

    def __init__(self, config_vars, name=None):
        self.config = config_vars
        self.gpio_config = config_vars['GPIO_CONFIGURATION']
        self.log = logging.getLogger(name or "MainController")

    def initialize(self):
        """Initialize the MainController. This activates the CW path."""
        self.initialize_hardware()
        self.button_cw_pressed()

    def initialize_hardware(self):
        """Initialize GPIO pins for LEDs and buttons."""
        # LEDs
        gpio_config = self.gpio_config

        self.led_pin    = LED(gpio_config['LED_PIN'])
        self.led_cw     = LED(gpio_config['LED_CW'])
        self.led_pepr   = LED(gpio_config['LED_PEPR'])

        # Buttons
        self.button_pin  = Button(gpio_config['BUTTON_PIN'], bounce_time=0.05,
                                       hold_time=5)
        self.button_cw   = Button(gpio_config['BUTTON_CW'], bounce_time=0.05)
        self.button_pepr = Button(gpio_config['BUTTON_PEPR'], bounce_time=0.05)

        # Button event handlers
        self.button_pin.when_pressed = self.button_pin_pressed
        self.button_pin.when_held    = self.button_pin_held

        self.button_cw.when_pressed    = self.button_cw_pressed
        self.button_pepr.when_pressed  = self.button_pepr_pressed

        # FEMTO amplifiers
        config = dict(coupling = gpio_config['FEMTO1_COUPLING'],
                      bandwidth= gpio_config['FEMTO1_BANDWIDTH'],
                      gain_lsb = gpio_config['FEMTO1_GAIN_LSB'],
                      gain_msb = gpio_config['FEMTO1_GAIN_MSB'],
                      gain     = gpio_config['FEMTO1_GAIN'])
        self.femto1 = FemtoAmplifier(config, name="Femto1")
        self.femto1.init()

        config = dict(coupling = gpio_config['FEMTO2_COUPLING'],
                      bandwidth= gpio_config['FEMTO2_BANDWIDTH'],
                      gain_lsb = gpio_config['FEMTO2_GAIN_LSB'],
                      gain_msb = gpio_config['FEMTO2_GAIN_MSB'],
                      gain     = gpio_config['FEMTO2_GAIN'])
        self.femto2 = FemtoAmplifier(config, name="Femto2")
        self.femto2.init()

        # PIN Switch
        config = dict(clear = gpio_config['PIN_CLR'],
                      set   = gpio_config['PIN_SET'],
                      clock = gpio_config['PIN_CLK'])
        self.pin_switch = PinSwitch(config, name="PinSwitch",
                                callback=self.pin_switch_state_changed)
        self.pin_switch.init()

        # Latching Switches
        config = dict(rf1 = gpio_config['SWITCH1_1'],
                      rf2 = gpio_config['SWITCH1_2'])
        self.switch1 = LatchingSwitch(config, name="Switch1",
                                callback=self.path_state_changed)
        self.switch1.init()

        config = dict(rf1 = gpio_config['SWITCH2_1'],
                      rf2 = gpio_config['SWITCH2_2'])
        self.switch2 = LatchingSwitch(config, name="Switch2")
        self.switch2.init()

    def button_pin_pressed(self):
        """Toggle the PIN switch by inverting the current state.

        In theory this could also be done by pulsing the CLK pin, but then we
        do not get any feedback about the current state.
        """
        self.log.debug("PIN button pressed, toggling...")
        self.pin_switch.state = not self.pin_switch.state

    def button_pin_held(self):
        """Reset the PIN switch to closed. """
        self.log.debug("PIN button held, closing switch...")
        self.pin_switch.state = False

    def button_cw_pressed(self):
        """Switch to CW mode."""
        self.log.debug("Enabling CW mode.")
        self.switch1.state = self.config['CW_PATH']
        self.switch2.state = self.config['CW_PATH']
        self.pin_switch.state = True

    def button_pepr_pressed(self):
        """Switch to PEPR mode."""
        self.log.debug("Enabling PEPR mode.")
        self.switch1.state = self.config['PEPR_PATH']
        self.switch2.state = self.config['PEPR_PATH']
        self.pin_switch.state = False

    def pin_switch_state_changed(self, state):
        self.led_pin.value = int(state)

    def path_state_changed(self, state):
        if state == self.config['CW_PATH']:
            self.led_cw.value = True
            self.led_pepr.value = False
        elif state == self.config['PEPR_PATH']:
            self.led_cw.value = False
            self.led_pepr.value = True

    def dispatch_request(self, message):
        """Dispatches a request and calls the appropriate function.

        Parameters
        ==========
        message:  The message to dispatch.

        Possible commands are given in the following list. Note that the
        commands are parsed case insensitive.

        - PATH <CW|PEPR>
          Sets the RF pathway to either the CW mode (direct out) or PEPR mode
          (downconversion active). Setting CW mode also activates the PIN
          switch, setting PEPR mode disables the PIN switch.

        - PIN <ON|OFF>
          Sets the state of the PIN switch manually to either ON or OFF.

        - EXTSWITCH <1|2>
          Explicitly sets the state of the external latching switch to either
          path 1 or 2, independent of chosen signal path.

        - FEMTO gain=<10..60>,coupling=<AC|DC>,bandwidth=<20,200>
          Configure the video gain amplifiers (FEMTO amplifiers).
        """
        command, args = message.split(' ', 1)
        command = command.strip().upper()
        if command == 'PATH':
            state = args.strip().upper()
            if state == 'CW':
                self.button_cw_pressed()
            elif state == 'PEPR':
                self.button_pepr_pressed()
            else:
                raise BadRequest('PATH: unknown path: {:s}'.format(state))

        elif command == 'PIN':
            state =args.strip().upper()
            if state == 'ON':
                self.pin_switch.state = True
            elif state == 'OFF':
                self.pin_switch.state = False
            else:
                raise BadRequest('PIN: unknown state: {:s}'.format(state))

        elif command == 'EXTSWITCH':
            state = int(args.strip())
            if 0 < state < 3:
                self.switch2.state = state
            else:
                raise BadRequest('EXTSWITCH: unknown state: {:s}'.format(args.strip()))

        elif command == 'FEMTO':
            args =  args.split(',')
            for arg in args:
                key, value = arg.strip().lower().split('=')
                if key == 'gain':
                    gain = int(value)
                    self.femto1.gain = gain
                    self.femto2.gain = gain
                elif key == 'coupling':
                    self.femto1.coupling = value.upper()
                    self.femto2.coupling = value.upper()
                elif key == 'bandwidth':
                    bandwidth = int(value)
                    self.femto1.bandwidth = value
                    self.femto2.bandwidth = value
                else:
                    raise BadRequest('FEMTO: unknown setting {:s}={:s}'
                                     .format(key, value))

        else:
            raise BadRequest('Unknown command: {:s}'.format(command))


class RequestHandler(socketserver.BaseRequestHandler):
    """Handles requests and dispatches to MainController."""

    def handle(self):
        main_controller = self.server.main_controller
        while True:
            data = self.request.recv(4096).strip()
            if not data:
                break
            # Commands are separated by \r\n
            data = data.decode("utf-8")
            messages = data.split('\r\n')
            for message in messages:
                try:
                    main_controller.dispatch_request(message)
                    self.response('OK\r\n')
                except (BadRequest, ValueError) as e:
                    self.response('ERR: {}\r\n'.format(e))

    def response(self, response):
        """Sends back a UTF-8 encoded response."""
        self.request.sendall(response.encode('utf-8'))


if __name__ == '__main__':
    import sys
    import config

    config_vars = {k: getattr(config, k) for k in dir(config)
                    if not k.startswith('__')}
    SERVER_ADDR = config_vars['SERVER_ADDR']

    # Set up logging
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s [%(levelname)-7s] %(name)16s: "
                                  "%(message)s")
    handler.setFormatter(formatter)
    root.addHandler(handler)

    log = logging.getLogger("Server")

    main_controller = MainController(config_vars)
    main_controller.initialize()

    server = socketserver.TCPServer(SERVER_ADDR, RequestHandler,
                                    bind_and_activate=False)
    server.allow_reuse_address = True
    server.main_controller = main_controller

    server.server_bind()
    server.server_activate()

    try:
        log.info("Starting server on port %d.", SERVER_ADDR[1])
        server.serve_forever()
    except KeyboardInterrupt:
        server.server_close()
        log.info("Server shutdown.")

