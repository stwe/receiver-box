# -*- coding: utf-8 -*-
#   receiver-box
#   Copyright (C) 2017 - 2019  Stefan Weichselbaumer
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Main configuration file for the receiver-box software"""

# IP and port to bind server to
SERVER_ADDR = '0.0.0.0', 5000

# GPIO configuration for the downconversion box
GPIO_CONFIGURATION = dict(
    # Status LEDs
    LED_PEPR           = 'GPIO7',
    LED_CW             = 'GPIO8',
    LED_PIN            = 'GPIO12',

    # Push buttons
    BUTTON_PEPR        = 'GPIO20',
    BUTTON_CW          = 'GPIO16',
    BUTTON_PIN         = 'GPIO21',

    # PIN Switch control circuit
    PIN_CLR            = 'GPIO2',
    PIN_CLK            = 'GPIO4',
    PIN_SET            = 'GPIO3',

    # Latching switches
    # TL Sep 20: Path IDs in code did not match the labels on Switch 2 -> Switched GPIOs
    # Switch 1 might be similarly wrong, but it is hardwired anyway.
    SWITCH1_1          = 'GPIO19',
    SWITCH1_2          = 'GPIO13',
    SWITCH2_1          = 'GPIO5',
    SWITCH2_2          = 'GPIO6',

    # FEMTO amplifier 1
    FEMTO1_GAIN_LSB    = 'GPIO27',
    FEMTO1_GAIN        = 'GPIO22',
    FEMTO1_GAIN_MSB    = 'GPIO10',
    FEMTO1_COUPLING    = 'GPIO17',
    FEMTO1_BANDWIDTH   = 'GPIO25',

    # FEMTO amplifier 2
    FEMTO2_GAIN_LSB    = 'GPIO18',
    FEMTO2_GAIN        = 'GPIO15',
    FEMTO2_GAIN_MSB    = 'GPIO14',
    FEMTO2_COUPLING    = 'GPIO23',
    FEMTO2_BANDWIDTH   = 'GPIO24',
)

# RF Paths through the receiver box
CW_PATH = 1
PEPR_PATH = 2

